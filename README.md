# README #

This README would normally document whatever steps are necessary to get your application up and running.

## What is this repository for? ###

This repository is used to generate the academy videos for ese > circles.

## How do I get set up? ###

Das Narakeet Client-Tool wurde installiert: https://www.narakeet.com/docs/automating/cli/


## Contribution guidelines ###

Bitte zuvor zur Qualitätsabstimmung mit den Verantwortlichen der Academy abstimmen.

## Verantwortlich ###

Bei Änderungen oder sonstigen Fragen an frank.szilinski@esentri.com wenden.


## Videos neu generieren
Dazu einfach ```./generate.sh``` ausführen. Die generierten Videos werden in den Ordner "generated" gespeichert.

## Informationen über die Ordner
* folie = Originale pptx-Folien zum exportieren als png
* out = Gerenderte Videos landen nach der Ausführung von generate.sh hier
* source = Alle notwendigen Dateien um die Videos zu generieren

## Änderungen im Text
Änderungen im Text können einfach im entsprechenden Kapitel in die Markdown-Datei geschrieben werden. Diese findet man direkt unter ```source/chapterXX.md```.

## Änderungen der Folien
Änderungen der Folien sollten in der originalen Powerpoint-Präsentation gemacht werden. Sind diese fertiggestellt, können die einzelnen Folien direkt über einen File>Export in das png-Format im jeweiligen Order eines Kapitels ```source/slides/chapterXX``` gespeichert werden.

## Stimmen ändern
Verfügbare Stimmen findet man hier: https://www.narakeet.com/docs/voices/#german

## Sonstige Markdown-Formatierungen
Die Formatierungen für das Markdown sind hier zu finden: https://www.narakeet.com/docs/format/#adjusting-background-music-volume


## Mögliche Contents

* Ziele Evolution
* Org-Struktur => DONE
* Plenum / Funktionen
* Kreise / Rollen + Aufgaben
* Kreis-Konstitution
* Dokumentation / nachlesen
* Delegationsmatrix
* Quality-Gates / Checklisten / Canvas
* Funktion SOK
* Beispiel neues Feature
