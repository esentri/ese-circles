---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/chapter05/Folie1.png)

(pause: 3)

Kommen wir zum letzten Kapitel, einer Übersicht über die notwendigen Unterlagen und Werkzeuge die uns zur Ausführung eines Auftrags zur Verfügung stehen. Klar ist, das gerade hier immer Bewegung im Spiel ist. 

(pause: 1)

Viele Herausforderungen müssen und können aber auch ohne Werkzeuge durchgeführt werden. Daher ist es wichtiger, das Ihr den gesamten Kontext versteht, um dann entsprechend handeln zu können.

(pause: 1)

---
![contain](slides/chapter05/Folie2.png)

Am wichtigsten sind die Unterlagen die es zum Auftrag gibt. Leider lassen sich nicht alle Details die Verhandelt oder angeboten wurden in einer Software abbilden. Daher ist es wichtig, das Ihr die von Sales zur Verfügung gestellten Dokumente versteht und auch prüft.

(pause: 1)

Das erste Dokument, ist natürlich das Angebot. Hier sollten die geforderten Skills oder die konkrete Kollegin oder der Kollege benannt sein. Zudem sind die Konditionen, die Kontingente und auch Laufzeit genannt. Manchmal benötig ein Kunde aber so schnell Unterstützung, das wir kein Angebot schreiben sondern der Kunde direkt bestellt.

(pause: 1)

Schwieriger wird es dann noch, wenn die Beauftragung vom Kunden erst nach dem offiziellen Auftragsstart bei uns ankommt. Hier ist Sales und der LAM aber im Bilde was geregelt und vereinbart wurde. Verlangt von Sales die wichtigen Informationen, damit Ihr ordentlich planen und prüfen könnt. Vorsicht, manchmal weichen die Konditionen und Kontingente von dem Angebot ab. Das passiert, wenn beispielsweise der Einkauf des Kunden das letzte Wörtchen zu reden hat.

(pause: 1)

Bei manchen Kunden oder wenn der Auftrag über einen Vermittler kommt, existiert ein Rahmenvertrag. Dann gibt es oft kein offizielles Angebot, da die Konditionen und die Stufen wie beispielsweise Junior, Senior oder das Skill-Set bereits dort geregelt sind.

(pause: 1)

---
![contain](slides/chapter05/Folie3.png)

(pause: 1)

Kommen wir zu den Werkzeugen. Diese sollen uns helfen, komplizierte Excel-Sheets und Checklisten zu vermeiden. Zudem ist die Abrechnung aller Zeiten von allen Kolleginnen und Kollegen ein nicht ganz so einfache Aufgabe. Dies muss dann noch unter einem enormen Zeitdruck geschehen, was in der Natur der Sache zu weiteren Fehlern führen kann. Daher ist es sehr wichtig, das die beteiligten Kreise und Personen sehr genau und auch aus unterschiedlichen Blickwinkeln die Korrektheit prüfen.

(pause: 1)

Unser älteres Werkzeug Lithosphere 1 hilft uns aktuell noch bei der Verarbeitung von Aufträgen. Hier können die Konditionen und Kontingente sowie Leistungsarten und Leistungspakete konfiguriert werde. Die erfassten Zeiten werden dann nach dem Controlling zur halbwegs automatischen Generierung von Rechnungen verwendet. Leider erfüllt das Tool noch nicht alle notwendigen Anforderungen für die verteilte Auftragsabwicklung.

(pause: 1)

In Personio können die Abwesenheiten, also Urlaub, Krankheit oder sonstige Ausfälle nachvollzogen werden.

(pause: 1)

Das neue Lithosphere 2 ist vor allem für die ökonomische und betriebswirtschaftliche Planung der Kreise vorgesehen. Planumsätze können hier nach dem Erhalt eines Auftrags entsprechend mit den realen Konditionen und Rahmenbedingungen eingeplant werden. Ein neuer Kreis-Forecast wird monatlich von jedem Kreis eingereicht.

(pause: 1)

Ihr könnt Euch aber auch eigene Werkzeuge und Stützen bauen. Wichtig ist nur, das alle Ihren Status und auch die Situation der Auftragslage kennen, und entsprechend kommunizieren. Mit einer offenen und transparenten Kommunikation, ist ein Controlling und eine Planung im Rahmen der Geschäftskreis Meetings schnell erledigt. Jeder kann hier helfen, ein gemeinsames Verständnis zu schaffen.

(pause: 1)

---