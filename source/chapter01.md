---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.02 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
voice-speed: 1.05
---
![contain](slides/chapter01/Folie1.png)

(pause: 3)

Bevor wir mit der Frage beginnen, was kollegiale Führung denn ist, wollen wir beleuchten, was es nicht ist.

(pause: 1)

Kollegiale Selbstorganisation ist aus unserer Sicht nicht zu verwechseln mit Beliebigkeit, Unverbindlichkeit, Hierarchie­freiheit, Willkür, Basisdemokratie, Endlosdiskussionen und Herrschaft des Mittelmaßes.

(pause: 2)

Ganz im Gegenteil. Gerade damit Führung zum verständlichen und alltäglichen Teil der Arbeit eines jeden Mitarbeiters werden kann, benötigen solche Organisationen eine belastbare und leistungsfähige soziale Architektur und Infrastruktur, einen klaren organisatorischen Rahmen und eine Reihe einfach zu benutzender Organisations- und Führungswerkzeuge.

---
![contain](slides/chapter01/Folie2.png)

Wir haben uns bei der Transformation zur Kollegialen Führung am Werkzeugkasten und Buch von Bernd Östereich und Claudia Schröder orientiert. Daneben gibt es weitere - wie hier abgebildet - sehr spannende Quellen, für ein Umdenken der Organisationsstrukturen.

(pause: 1)

Die Transformation selbst beinhaltet aber weitaus mehr Themen, als nur die Organisationsstruktur zu beleuchten. 

(pause: 1)

Neben der eigentlichen Organisationsstruktur, also der Bezeichnungen, Funktion und Ausrichtungen von Teams und den Rahmenbedingungen der Hierarchie, geht es vor allem um die Spielregeln, also Prinzipien und Prozesse. 

(pause: 1)

Es ist aber auch wichtig, das unsere Organisation sich der stetig wandelnden Umwelt und an den Markt anpassen kann. Die Organisation muss in der Lage sein, sich auch dezentralisiert verändern zu können. Dazu benötigen wir Regeln und Prinzipien, wie wir die Veränderung am Unternehmen gestalten möchten.

(pause: 1)

Stellen wir uns kurz für einen Augenblick vor, die Organisation selbst ist wie eine große Software. 

(pause: 1)

Änderungen - oder Features - am und für das Unternehmen, durchlaufen dann einen Entscheidungs-, Entwicklungs- sowie Prüfungsprozess. Ist die Änderung am Unternehmen getestet oder verprobt, kann diese eingespielt werden. Änderungen am Unternehmen können daher beispielsweise auch wie bei der Software-Entwicklung in einem Release-Lock für alle transparent gemacht werden. Wie in der Software-Entwicklung auch, benötigen wir für Änderungen am Unternehmen entsprechende Entscheidungsverfahren.

(pause: 1)

Kollegiale Führung ist in erster Linie vor allem aber auch eine Änderung der denkweise. Wir haben damals im Jahr 2019 mit der Transformation des Unternehmens begonnen. Die grundsätzliche Leitlinie war, eine Unternehmensstruktur zu schaffen, in der Kreativität, Selbstbestimmung, Transparenz und Flexibilität genügend Raum und Platz finden kann. 

(pause: 1)

Dafür benötigt es ein klares Führungsverständnis und eine koordinierte und strukturierte Zusammenarbeit aller Kolleginnen und Kollegen.

---
![contain](slides/chapter01/Folie3.png)

Die Werkzeuge der Kollegialen Führung haben viele Mütter und Väter und ist eine Synthese ganz unterschiedlicher, sich aber ergänzender Ansätze. Einige sind viele Jahrzehnte alt, andere sind erst in jüngerer Zeit bekannt oder relevant geworden.

(pause: 1)

Einige der wichtigsten Einflüsse und daher erwähnenswert sind zum Beispiel die Theorie U von Otto Scharmer sowie die sozialtechnologie der Holokratie von Brian Robertson. Die Holokratie beschreibt beispielsweise den Aufbau von Abteilungen in Kreisen und Rollen. Aber auch das Agile Manifest sowie Lean Strategie spielen eine wichtige Rolle. 

(pause: 1)

Es ist natürlich nicht sinnvoll, alle Einflüsse im Detail zu erklären, noch ist es nötig diese alle zu kennen. Dennoch kann es positiv zur eigenen und persönlichen Entwicklung nützlich sein.

(pause: 3)

---
![contain](slides/chapter01/Folie4.png)

Lesen wir zunächst eine Definition der Kollegialen Führung vor. 

(pause: 1)

Kollegiale Führung ist die auf viele Kollegen und Kolleginnen dynamisch und dezentral verteilte Führungsarbeit an Stellen von zentralisierter Führung durch exklusive Führungskräfte.

(pause: 1)

Wir möchten nun die einzelnen Bausteine in einzelnen Abhandlungen erklären.

---
![contain](slides/chapter01/Folie5.png)

Was bedeutet also, Kollegiale Führung im einzelnen?

(pause: 1)

Kollegial bedeutet unter Kollegen und Kolleginnen und beschreibt das Grundprinzip, wie Führung entsteht.

(pause: 1)

Agil kann eine Wirkung sein, Kreisstrukturen ein Mittel.

(pause: 2)

---
![contain](slides/chapter01/Folie6.png)

Was bedeutet aber das Wort viele?

(pause: 1)

Kollegiale Führung verkraftet es gut, wenn einige Kollegen mal oder immer geführt werden wollen. 

(pause: 1)

Nicht jeder möchte immer, bei allen Gelegenheiten oder nur zu bestimmten Themen führen und entscheiden.

(pause: 2)

---
![contain](slides/chapter01/Folie7.png)

Kollege ist jeder, der innerhalb der Organisation mitarbeiter, egal ob angestellter Mitarbeiter, Inhaber, Auszubildende, Werksstudenten oder Praktikanten.

(pause: 1)

Wir bezeichnen alle Mitarbeiter auch als das Plenum.

---
![contain](slides/chapter01/Folie8.png)

Dynamisch bedeutet, dass je nachdem, welche Fähigkeiten ein Kontext erfordert und wer diese dafür bieten kann, Kolleginnen und Kollegen sich in der Sache einbringen dürfen.

(pause: 1)

Dabei kann einer Person die Zuständigkeit für einen Bereich und einen längeren Zeitraum übertragen werden ebenso wie eine einmalige Entscheidungsaufgabe erhalten.

---
![contain](slides/chapter01/Folie9.png)

Dezentral verteilt bedeutet nicht unten oder oben ist relevant, sondern wer für welche Führungs- und Entscheidungsbedarfe der jeweils passende ist.

---
![contain](slides/chapter01/Folie10.png)

Zur Führungsarbeit gehören nach außen gerichtete operative und strategische Entscheidungen ebenso wie organisationale Entscheidungen zur Weiterentwicklung der internen Zusammenarbeit und Organisation.

---
![contain](slides/chapter01/Folie11.png)

Je dynamischer und komplexer das Geschäft und die Organisation wird, desto weniger sind einzelne zentrale Akteure in der Lage, allein sinnvoll zu entscheiden und desto wichtiger wird die hierarchieübergreifende Kooperation.

---
![contain](slides/chapter01/Folie12.png)

Führungskräfte gibt es auch in der Kollegialen Führung, aber nicht mehr vorgesetzt, exklusiv und unbefristet, sondern situativ. Jeder Kollege ist mehr oder weniger auch Führungskraft, als selbstverständlicher Teil seiner Arbeit.

(pause: 1)

Dennoch ist es auch sinnvoll, vor allem für primäre Kreise, in der Kolleginnen und Kollegen hauptsächlich angehören, eine längerfristige Führungsarbeit zu verankern. Ein Repräsentant oder Repräsentantin eines Kreises, sollte diesen auch in der visionären Ausrichtung und operativen Steuerung längerfristig führen.

---
![contain](slides/chapter01/Folie13.png)

In diesem Kapitel haben wir gezeigt, aus welchen Gründen wir uns damals in 2019 für die Kollegiale Führung entschieden haben und welche Zielsetzungen wir mit der Transformation des Unternehmens verfolgen.

(pause: 1)

Wir haben auch kurz die wichtigsten Einflüsse der Kollegialen Führung kennengelernt.

(pause: 1)

Danach sind wir auf die formale Definition der Kollegialen Führung eingegangen und haben die einzelne Begriffe kurz beleuchtet.