---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/chapter04/Folie1.png)

(pause: 3)

In diesem Abschnitt möchten wir Euch aus der Perspektive der Kreise erklären, wer welche Aufgaben hat, um eine Expertendiensleistung durchzuführen.

---
![contain](slides/chapter04/Folie2.png)


Der Geschäftskreis Lead übernimmt die Verantwortung, für die fristgerechte Ausführung des Auftrags und die kreisinterne Steuerung. Er übernimmt den Auftragseingang vom Dienstleistungskreis Sales und führt das OnBoarding mit den betroffenen Kollegen und Kolleginnen durch. In regelmäßigen Abständen bespricht der Geschäftskreis die Ergebnisse und kontrolliert die Zeiten sowie die Abrechnung. Neue Beauftragungen plant der Geschäftskreis in einer neuen Budget-Version ein und stellt die korrekte inhaltliche Abrechnung sicher. 

(pause: 1)

Im Controlling werden die erbrachten Zeiten sowie die laufende Kontingente und Laufzeiten geprüft und ggf. bei Unter- oder Überschreitung an Sales gemeldet. Auch eventuell auftretende Mängel oder Qualitätsthemen werden gemeinsam mit Sales besprochen und mit dem Kunden für die Abrechnung geklärt.

(pause: 1)

Der Geschäftskreis muss die Auslastung und die zukünftige Auftragslage im Blick behalten und entsprechende Maßnahme definieren um Engpässe, also Unter- oder Überlastungen zu vermeiden.

(pause: 1)

Die Aufgaben können nach einem Rollen-Split auf unterschiedliche Schultern verteilt werden. Das ist aber genauer im Rollenkonzept erklärt.

(pause: 1)

---
![contain](slides/chapter04/Folie3.png)

(pause: 1)

Die Kollegin oder der Kollege des Geschäftskreises erzeugt beim Kunden den Vorsprung und erfasst täglich die Zeiten. Sie prüfen selbstständig Ihren Auftrag und informieren den Geschäftskreis Lead über die laufende Tätigkeit und sonstige Möglichkeiten, die beim Kunden erkannt werden. Jeder sollte die Aufgaben aus dem fünf Finger Prinzip kennen.

(pause: 1)

Denkt an der korrekten Kommunikation, wenn es um Abwesenheiten und Krankheiten geht. Helft dem Geschäftskreis dabei, die Auslastung und Auftragslage korrekt zu planen.

(pause: 1)

---
![contain](slides/chapter04/Folie4.png)

Der Dienstleistungskreis Sales übernimmt die Vorbereitungen für ein Angebot und Koordiniert die Szenarien mit Euch noch vor dem eigentlichen Auftragseingang. Ein Lead Account Manager von Sales wird dann auch den Auftragseingang überwachen und Euch die notwendigen Informationen übergeben.

(pause: 1)

Sales und der entsprechende LAM wird Euch auch weiterhin als Ansprechpartner zur Verfügung stehen. Welcher Lead Account Manager für welchen Auftrag oder Kunde verantwortlich ist, erfahrt Ihr dann vom Dienstleistungskreis Sales.

(pause: 1)

Falls ein Auftrag nicht verlängert wird oder offiziell nach der Laufzeit ausläuft, dann kümmert sich Sales auch weiterhin um neue Möglichkeiten und Szenarien. In einem anderen Organisationsfeature erklären wir Euch, wie wir gemeinsam am Auftragsmarktplatz passende Aufträge für alle finden können.

(pause: 1)

---
![contain](slides/chapter04/Folie5.png)

Die sonstigen Dienstleistungskreise wie Operations, Finance und IT, kümmern sich um die Dinge, die im Hintergrund passieren müssen. Dort werden die Stammdaten gepflegt und auch die richtigen Ansprechpartner gesucht, wenn es um die Abrechnung und sonstige Details geht. Sie kümmern sich auch darum, das Eure Bedürfnisse in unserer IT Landschaft erfüllt werden können.

(pause: 1)

---
