---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.02 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
voice-speed: 1.05
---
![contain](slides/chapter02/Folie1.png)

(pause: 3)

In diesem Kapitel möchten wir Euch zeigen, wie unsere Organisation aufgebaut ist. Wir werden die unterschiedlichen Kreistypen vorstellen und wie Kolleginnen und Kollegen der einzelnen Kreise miteinander vernetzt sind.

---
![contain](slides/chapter02/Folie2.png)

Beginnen wir mit der initialen Idee für unsere Organisationsstruktur.

(pause: 1)

[People don’t buy what you do.]{en}

(pause: 1)

[They buy why you do it.]{en}

(pause: 1)

Dieser Satz beschreibt den Kern des Golden Circle Modells, das der Unternehmensberater und Autor Simon Sinek entwickelt hat. Sinek erklärt darin, warum es manche Unternehmen schaffen, andere Menschen zu inspirieren und andere nicht. Es liegt daran, dass diese vor allem ihr why kennen.

(pause: 1)

Hier steht die Frage nach dem Zweck, dem Grund, dem Glauben, den das Unternehmen verfolgt. Dies wird natürlich vor allem durch die Innhaber und Vorstände vertreten. Dennoch müssen sich viele Personen des Unternehmens koordinieren und auch beispielsweise gemeinsam an der Strategie und Ausrichtung arbeiten.

---
![contain](slides/chapter02/Folie3.png)

Beim nächsten Ring geht es darum, wie wir das Why umsetzen. 

(pause: 1)

Die Frage zielt dabei zum Beispiel auf die Werte, die wir unseren Kunden versprechen oder unser Alleinstellungsmerkmal ab.

(pause: 1)

Im übertragenen Sinne kann man daher für die Organisationsstruktur sagen, das in diesem Ring die Dinge passieren müssen, die für einen reibungslosen Ablauf sorgen.

---
![contain](slides/chapter02/Folie4.png)

Im äußersten Ring, geht es um die Frage, was wir machen. Das ist im Normalfall recht schnell und einfach zu erklären. 

(pause: 1)

Denkt man dabei an die Organisationsstruktur, so sollte man hier also die Dinge finden, die wir auch nach aussen gut sichtbar machen können. Dazu gehört das Portfolio einzelner Kreise, die Technologien und Fähigkeiten, mit denen wir dem Markt helfen, Ihre nachhaltige Digitalisierungsstrategien umzusetzen.

---
![contain](slides/chapter02/Folie5.png)

Wir vergrößern die Abbildung und teilen Sie in zwei Bereiche ein. 

(pause: 1)

Links sprechen wir über den Markt, für den wir Angebote stellen. Das sind beispielsweise unsere Kunden.

(pause: 1)

Rechts sprechen wir über den Markt, den wir für unsere Entwicklung und Sichtbarkeit nutzen. Dazu gehören beispielsweise unsere Duesentrieb Camps, Konferenzen oder sonstige Partnerunternehmen, die uns unterstützen.

(pause: 1)

Dies dient grundsätzlich nur zur Orientierung und hat keine weitere Funktion.

---
![contain](slides/chapter02/Folie6.png)

Beginnen wir mit dem Innhaberkreis.

(pause: 1)

Dieser Kreis wird durch das Aktiengesetz geregelt und Mitglieder sind die Aktionäre des Unternehmens. Somit haben die Aktionäre einen grundlegenden Einfluss auf die Unternehmens- und Führungskultur. Es wird mindestens ein mal im Jahr eine Hauptversammlung der Aktionäre durchgeführt.

---
![contain](slides/chapter02/Folie7.png)

Der Strategie und [Visionskreis]{de} beeinflusst die Kommunikation und praktischen Handlungen im Unternehmen im Hinblick auf die grundsätzliche Entwicklungsrichtung der Organisation. Es ist grundsätzlich kein hierarchieorientiertes Gremium, sondern ein kleiner Kreis von ausgewählten Personen. 

(pause: 1)

Der Vorstand und die erweiterte Geschäftsführung ernennt daher Personen, die bekanntermaßen eine eigene Strategie engagiert und überzeugt vertreten. 

(pause: 1)

Mit unterschiedlichen Hilfsmitteln wie beispielsweise dem [Business Model Canvas]{en}, der Trend Beobachtung und einer kontinuierlichen Marktbeobachtung, wird in diesem Kreis die Unternehmensstrategie entschieden und auch auf eine Steuerungsebene gebracht.

(pause: 1)

Ein Ergebnis des Kreises, ist es dabei, zum Beispiel Ziele, die sich an der Strategie orientieren, für die Kreise der Organisation zu entwickeln.

---
![contain](slides/chapter02/Folie8.png)

Ein weiterer Kreis ist zum Beispiel das [supervisory board]{en}, also der Aufsichtsrat.

(pause: 1)

Dieser richtet seine Arbeit nach dem Aktiengesetz aus und unterstützt das Unternehmen bei der Bewertung von Strategien, Risiken und Chancen. Dabei haben die Aufsichtsräte natürlich auch die Aufgabe, Entscheidungen kritisch zu hinterfragen und zu prüfen.

---
![contain](slides/chapter02/Folie9.png)

Der nächste Kreis, das [executive board]{en} wird von den Vorständen geleitet und von den Aufsichtsräten kontrolliert. 

(pause: 1)

Der [Chief Executive Officer]{en} ist automatisch der Repräsentant dieses Kreises und daher auch Repräsentant des gesamten Unternehmens. In diesem Kreis werden die formellen Vorstandssitzungen durchgeführt. Vorstände werden vom Aufsichtsrat für jeweils drei Jahre bestellt.

---
![contain](slides/chapter02/Folie10.png)

Wenn man die Verbindungen zwischen den Kreisen betrachtet, nehmen die Vorstände sowie die extern berufenen Aufsichtsräte an den Aufsichtsratssitzungen teil. 

---
![contain](slides/chapter02/Folie11.png)

Alle drei bisher dargestellten Kreise werden grundlegend als Koordinationskreise bezeichnet. 

(pause: 1)

Koordinationskreise sind Sekundärkreise. Zwischen Kolleginnen und Kollegen sowie den Koordinationskreisen gibt es kein arbeitsvertragliches Verhältnis.

(pause: 1)

Koordinationskreise sind zweckgebunden und ermöglichen neben der Führung und Kommunikation zwischen Teilnehmern aus einzelnen Organisationseinheiten eine fest vereinbarte Arbeitsweise.

---
![contain](slides/chapter02/Folie12.png)

Kommen wir zum mittleren Ring. Hier ist beispielsweise der Kreis [Human Relations]{en} angesiedelt. 

---
![contain](slides/chapter02/Folie14.png)

Die Kreise in diesem Ring nennen wir Dienstleistungskreise.

(pause: 1)

Diensleistungskreise sind im Gegensatz zu den Koordinationskreisen primäre Kreise. Kolleginnen und Kollegen des Kreises sind hier angestellt.

(pause: 1)

Ein weiteres Beispiel ist der Dienstleistungskreis Finance, der sich um die Planung und Steuerung der Finanzen des Unternehmens kümmert. Er unterstützt Kreise mit der Abrechnung von Leistung, versorgt diese mit Kennzahlen und kümmert sich um die Einhaltung von Regularien und Prozessen.

(pause: 1)


(pause: 1)

Im Fall von [human relations]{en} ist es beispielsweise die Ablage von Akten der Kolleginnen und Kollegen, sowie die Steuerung und Kontrolle der Lohnabrechnungen. Auch das [recruiting]{en} wird hier zentralisiert budgetiert und ausgesteuert.

---
![contain](slides/chapter02/Folie15.png)

Im äußersten Ring sind die Kreise aufgeführt, die ein Angebot an den Kunden oder Unterstützenden Markt haben und gleichzeitig an der direkten Wertschöpfung beteiligt sind.

(pause: 1)

Hier im Beispiel nehmen wir den Kreis Architektur. Er bietet Kunden ein Leistungsportfolio an, welches sich auf Architekturaufgaben bezieht. Er kann dem Kunden beispielsweise Kolleginnen oder Kollegen mit spezialisierten Fachrollen wie Lösungsarchitekt oder Unternehmensarchitekt anbieten.

---
![contain](slides/chapter02/Folie16.png)

Ein weiteres Beispiel ist der Kreis Integration, mit spezialisierten Kolleginnen und Kollegen, die sich darum kümmern, Schnittstellen, Interfaces oder Prozessorchestrierung für Kunden zu realisieren.

---
![contain](slides/chapter02/Folie17.png)

Der erste Kreistyp auf dem äußersten Ring bezeichnen wir Geschäftskreise.

(pause: 1)

Da Geschäftskreise wie auch Dienstleistungskreise Primärkreise sind, werden Kolleginnen und Kollegen darin angestellt und somit auch beispielsweise deren Gehälter und Entwicklungsmaßnahmen eingeplant.

---
![contain](slides/chapter02/Folie18.png)

Neben den spezialisierten Leistungsangeboten der Geschäftskreise - auch als ese expert services genannt - gibt es natürlich auch Projekte, in denen Kolleginnen und Kollegen aus den einzelnen Geschäftskreisen zusammen Lösungen für unsere Kunden erarbeiten.

(pause: 1)

Hier dargestellt ein Beispielprojekt mit dem Namen [new portal]{en}.

---
![contain](slides/chapter02/Folie19.png)

Der Kunden des Projektes ist die [ligth bulb inc.]{en}.

---
![contain](slides/chapter02/Folie20.png)

Dieses Unternehmen ist der Auftraggeber für das Projekt.

(pause: 1)

Da im Beispiel für die Realisierung des Angebotes Fähigkeiten aus unterschiedlichen Geschäftskreisen benötigt werden, entsteht in unserer Organisation ein neuer Kreis.

(pause: 1)

Diese nennen wir Projektkreise.

---
![contain](slides/chapter02/Folie21.png)

Die zugeordnete Mitglieder der Geschäftskreise, sind somit in dem neuen Projektkreis mit Ihrer Fachrolle zugeordnet. In einem Projektkreis sollte wie auch in anderen Kreisen, ein Repräsentant ernannt werden. Dieser ist dann für die Durchführung und Steuerung des Projektes verantwortlich.

---
![contain](slides/chapter02/Folie22.png)

Neben externen Projekten, die durch Kunden beauftragt werden, gibt es natürlich auch interne Projekte wie beispielsweise die Durchführung einer Konferenz.

---
![contain](slides/chapter02/Folie23.png)

Auch hier können, wie bei den Kundenprojekten, Kolleginnen und Kollegen aus den anderen primären Kreisen teilnehmen. 

(pause: 1)

Projekte, egal ob für Kunden oder als Enabler, sind zeitlich immer terminiert und sollten vor allem für eine transparente und koordinierte Zusammenarbeit konstituiert werden.

(pause: 1)

Weitere Projektarten können beispielsweise Forschungsprojekte, Supportprojekte, strategische Initiativen oder Maßnahmen sein.

---
![contain](slides/chapter02/Folie24.png)

Die exemplarische Konferenz wird durch Kolleginnen und Kollegen aus den Geschäfts- sowie Dienstleistungskreisen organisiert. Aus dem Umfeld und Markt können dann weitere Teilnehmer und Partner teilnehmen oder eingebunden werden. 

(pause: 1)

Wichtig ist, wie auch in den anderen Kreisen, das es einen Repräsentant und somit primären Ansprechpartner für den Projektkreis gibt, der sich um die Konstitution und die inhaltliche Organisation des Kreises kümmern muss.

---
![contain](slides/chapter02/Folie25.png)

Wir machen ein weiteres Beispiel und listen diverse andere Geschäftskreise des Unternehmens auf.

---
![contain](slides/chapter02/Folie26.png)

Damit nicht nur die Ziele und Strategie gerichtet und abgestimmt stattfinden kann, treffen sich alle Repräsentanten in einem vereinbarten Rhytmus im Koordinationskreis GK Board. 

(pause: 1)

Hier koordinieren und synchronisieren die Sprecher der einzelnen Geschäftskreise also Themen, die nur gemeinsam gelöst werden können. 

(pause: 1)

Auch in Koordinationskreisen sollte die Repräsentantenrolle besetzt werden, damit der Kreis aussagefähig ist und auch organisiert stattfinden kann.

---
![contain](slides/chapter02/Folie27.png)

Exemplarisch listen wir daher auch die weiteren Dienstleistungskreise auf. 

(pause: 1)

Der Leistungskatalog, die Mitglieder, die Vision und Mission sowie die vereinbarten Arbeitsprozesse und Termine werden in allen Kreisen für alle dokumentiert und transparent gemacht.

(pause: 1)

Somit können alle Kolleginnen und Kollegen des Unternehmens auch nachvollziehen, was die einzelnen Kreise tun.

---
![contain](slides/chapter02/Folie28.png)

In regelmäßigen Abständen treffen sich auch die Repräsentanten der einzelnen Dienstleistungskreise genau wie die Repräsentanten der Geschäftskreise in einem Koordinationskreis, um beispielsweise Änderungen in den Werkzeugen,Schnittstellen, Formalien oder Prozessen zu besprechen.

---
![contain](slides/chapter02/Folie29.png)

Kommen wir noch zu einem sehr spezialisierten Kreistyp, der Kollegengruppe.

(pause: 1)

Die Kollegengruppe wird aus unterschiedlichen Kolleginnen und Kollegen des Unternehmens gebildet. Normalerweise ist sie in einer größe von 3 bis 5 Kolleginnen, die sich im Grundsatz um Arbeitgeberaufgaben kümmern. 

---
![contain](slides/chapter02/Folie30.png)

Dies könnte wie hier im Beispiel eine Gruppe von Kolleginnen und Kollegen sein, die andere Kreise in der Organisationsrolle als Lernbegleiter für das Thema `OKR` und Ziele unterstützen kann.

---
![contain](slides/chapter02/Folie31.png)

Alle Teilnehmer der Gruppe spezialisieren sich Themenspezifisch, hier im Beispiel als `OKR` Coaches, und kümmern sich auch um die Weiterentwicklung des Angebotes.

(pause: 1)

Ein weiteres Beispiel könnte eine Kollegengruppe sein, die beispielsweise bei der Erstellung von Arbeitszeugnissen unterstützt oder eine Gruppe, die sich regelmäßig zur Analyse von Trendthemen trifft.

(pause: 1)

Eine Abgrenzung zwischen Kollegengruppen, Dienstleistungskreisen und Koordinationskreisen ist manchmal etwas schwieriger. Grundsätzlich sind alle zusätzlichen Dienstleistungsangebote, die zwar einen kontinuierlichen aber eher punktuellen und koordinativen Charakter haben, als Kollegengruppe zu konstituieren.

---
![contain](slides/chapter02/Folie32.png)

Eine weitere spezialisierte Form eines Kreises sind die Praktikergruppen.

(pause: 1)

Sie sind offene Praktikergemeinschaften ohne Entscheidungsbefugnisse, von Spezialisten eines Fachgebietes oder von Inhabern der gleichen Rolle, um gemeinsam Erfahrungen und Wissen auszutauschen.

(pause: 1)

Es geht im Kern darum, miteinander zu lernen und fachliche Ausrichtungen organisationsintern konvergieren zu lassen.

---
![contain](slides/chapter02/Folie33.png)

Hier wäre beispielsweise eine Praktikergruppe für Amazon Web Services denkbar. 

(pause: 1)

Die Konstitution sollte neben dem Themengebiet vor allem die Treffen und Beitrittsmöglichkeiten aufzeigen. Wer lust hat und teilnehmen möchte, der sollte auf diesen Weg erstens das Angebot finden können, und zweitens wissen, wie, wann und wo die Teilnahme möglich ist.

---
![contain](slides/chapter02/Folie34.png)

Als letztes möchten wir noch erwähnen, wie Geschäftseinheiten konstruiert werden.

(pause: 1)

Eine Geschäftseinheit ist ein rein logisches konstrukt und hat keine weitere Funktion. Dennoch ist es praktisch, alle Zusammenhängenden Einheiten einem übergeordneten Thema zuordnen zu können.

(pause: 1)

Hier im Beispiel wird die Geschäftseinheit Forschung und Entwicklung gezeigt. Diese besteht nicht nur aus einem Dienstleistungskreis, der kontinuierlich beispielsweise das Thema Trendbeobachtung durchführt und bei der Erstellung von Forschungsanträgen unterstützt, sondern auch aus diversen internen Forschungsprojekten und strategischen Maßnahmen. Ebenso ist eine Kollegengruppe, eine Praktikergruppe und ein Koordinationskreis für alle Forschungsprojekte entstanden.


---
![contain](slides/chapter02/Folie36.png)

Wir möchten das Kapitel nochmals kurz zusammenfassen. 

(pause: 1)

Wir haben Euch gezeigt, warum der Golden Circle von Sinek und die Kollegiale Führung sehr eng aneinander liegen. 

(pause: 1)

Wir haben das Konzept der Koordinationskreise vorgestellt, die vor allem für einen Informationsaustausch und eine gemeinsame Entscheidungsfindung wichtig sind.

(pause: 1)

Dienstleistungkreise helfen mit Ihrem Leistungsangebot anderen Kreisen dabei, sich auf Ihre eigentliche Wertschöpfung konzentrieren können.

(pause: 1)

Geschäftskreise orientieren sich an einem speziellen Thema und dem damit verbundenen Leistungsangebot für Kunden. Geschäftskreise können dem Markt über den Auftragstyp expert services Leistungen anbieten.

(pause: 1)

Projektkreise bilden eine temporäre Struktur um die Herausforderung oder Angebote von Kunden umsetzen zu können. Die Mitglieder werden je nach Zielsetzung aus den anderen Kreisen zugeordnet. Es gibt aber auch interne Projektkreise die für eine spezielle Aufgabe formiert werden.

(pause: 1)

Kollegengruppen bieten eine spezielle und arbeitgebernahe Leistung an. Dabei unterstützen diese mit Lernbegleitern oder verfolgen eine spezifische und kontinuierliche Aufgabe.

(pause: 1)

In Praktikergruppen treffen sich die Kolleginnen und Kollegen regelmäßig, um einen informellen und wissensorientierten Austausch zu schaffen. Kolleginnen und Kollegen beispielsweise mit der selben Rolle oder den selben technologischen oder methodischen Herausforderungen.

(pause: 1)

Eine Geschäftseinheit ist eine logische Klammer um mehrere Kreise und bietet eine zusätzliche themenbezogene Orientierungshilfe an.

(pause: 1)

In primären Kreisen sind die Kolleginnen und Kollegen angestellt. Dazu zählen Geschäfts- sowie Dienstleistungskreise. Sekundärkreise bilden sich immer aus dem Netzwerk unterschiedlicher Personen zweckgebunden aus.

(pause: 1)

Wir haben auch ein paar Beispiele gezeigt, wie sich vor allem die sekundären Kreise aus Mitgliedern der anderen Kreise zusammensetzen kann. Dabei bildet sich mit den unterschiedlichen Konstellationen eine Netzwerkstruktur.

(pause: 1)

Grundlegend ist die Typisierung der Kreise sehr wichtig, um den Mitgliedern und auch dem Plenum das why zu geben. Es ist neben der thematischen Strukturierung somit wesentlich einfacher, die Arbeit eines Kreises in der gesamten Organisation einzuordnen. Wir schaffen also mehr Transparenz.

