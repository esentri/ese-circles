---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.02 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
voice-speed: 1.05
---

![contain](slides/chapter03/Folie1.png)

Wir haben im vorherigen Kapitel die Organisationsstruktur auf Makro-Ebene kennengelernt. Kommen wir nun zur Fragestellung, wie ein einzelner Kreis im inneren, also auf Mikro-Ebene aufgebaut ist.

---
![contain](slides/chapter03/Folie2.png)

Im Bezug auf die vorherige Organisationsstruktur möchten wir nun als Beispiel den Kreis User Experience und Interfaces beleuchten.

(callout: 
  cx: 558 
  cy: 751 
  size: 100)

(pause: 1)

---
![contain](slides/chapter03/Folie3.png)

Dem Geschäftskreis gehören 8 Kolleginnen und Kollegen an. 

(pause: 1)

Tom ist heute der Kreis-Lead und berichtet an den Chief Technical Officer Frangalf. Mit Ihm gleicht er seine eigene Vision und Strategie ab und gemeinsam besprechen Sie die Ziele und das Leistungsportfolio. Frangalf ist also sein direkter Vorgesetzter und auch primärer Ansprechpartner.

---
![contain](slides/chapter03/Folie4.png)

Tom hat als Kreis-Lead die Aufgabe und Verantwortung, die operativen Dinge, die Funktionsfähigkeit und Ausrichtung des Kreises regelmäßig zu hinterfragen und auf den Markt auszurichten. 

(pause: 1)

Dabei muss er zwei Perspektiven einnehmen. Erstens, die Perspektive des Leads. 

(pause: 1)

Die grundsätzliche Aufgabe des Leaderships könnte man kurz so definieren: Er beobachtet für das Themengebiet des Geschäftskreises den Markt und prüft die Bedarfe. Er muss daher auch konsequent das Leistungsangebot visionär und zielgerichtet vorantreiben.

(pause: 1)

Die zweite Perspektive ist die einer Führungskraft.

(pause: 1)

Als Führungskraft hat er vor allem die Aufgabe, die Kolleginnen und Kollegen in Ihrer Entwicklung zu führen und zu stärken. Dabei muss er die operative Herausforderungen geschickt und kontinuierlich steuern und lenken.

---
![contain](slides/chapter03/Folie5.png)

Die Rolle von Tom, als Kreis-Lead, beinhalten also Aufgaben, die er als Repräsentant und Visionär zu leisten hat. Daneben ist er für die ökonomische und finanzielle Struktur und Steuerung verantwortlich. Als zusätzliches Element muss er sich als Gasgeber um die emotionale Stabilität und die Einhaltung von geschaffenen Strukturen im Kreis kümmern.

---
![contain](slides/chapter03/Folie6.png)

Tom ist aber auch ein sehr guter Entwickler von User Interfaces.

(pause: 1)

---
![contain](slides/chapter03/Folie7.png)

Diesen Spagat hält Tom leider nicht lange aus. Er ist in einem ständigen hin und her zwischen Rollen und Perspektiven. Er hat kaum noch Zeit das eine oder das andere richtig zu machen.

---
![contain](slides/chapter03/Folie8.png)

Tom nutzt unsere Kreisstruktur und gibt die Rolle des Gastgebers an Paul ab. Er möchte aber weiterhin als Visionär den Geschäftskreis repräsentieren.

(pause: 1)

Da Ihm generell die Planung und das Controlling von Finanzen liegt, behält er diese Rolle vorerst auch.

(pause: 1)

Nach einigen Wochen spürt Tom aber, das es weitere Themen gibt, die er für den Kreis leisten muss. Wer kümmert sich um das Job-Profil für den neuen gesuchten Kandidat oder Kandidatin? Wer sorgt dafür, das die Profile der Kolleginnen und Kollegen aktuell bleiben? Wer kann den intensiver mit Sales im Austausch über die aktuell angestrebte Strategie bleiben und den Markt weiterhin beobachten?

---
![contain](slides/chapter03/Folie9.png)

Tom beschließt, seinen Kreis vollständig auf die Kollegiale Führung umzustellen. Dabei gibt er den Kolleginnen und Kollegen die freie Wahl nach zusätzlichen Themen, die Sie in Zukunft eigenverantwortlich betreuen können. 

(pause: 1)

Recht schnell ist klar, das sich fast alle intensiver mit den Aufgaben und Herausforderungen beschäftigen und einbringen möchten, die Tom bisher allein gemeistert hat.

(pause: 1)

Sie stellen sogar fest, das Ida ein unglaubliches Talent bei der Suche und Ansprache von potentiellen neuen Kolleginnen und Kollegen hat.

---
![contain](slides/chapter03/Folie10.png)

Ebenso stellen alle fest, das Kim eine absolute Top-Expertin für alle Adobe Produkte ist. In regelmäßigen Abständen wird sie von vielen anderen Kolleginnen und Kollegen des Unternehmens mit Fragestellungen und Hilferufe konsultiert.

(pause: 1)

Der Kreis beschließt, Kim eine sehr spezielle Organisationsrolle zu geben. Nämlich die der Adobe Expertin. Diese lokalen und spezifischen Rollen werden auch Spezifikas genannt.

---
![contain](slides/chapter03/Folie11.png)

Der Kreis hat auch im regelmäßigen Meeting erkannt, dass sie mit den neuen Zielen des Vorstandes noch nicht so richtig klar kommen. Tom, der eigentlich in der Vergangenheit immer so getan hat, als ob er auf alles eine Antwort wüsste, hat umgehend Ella aus der Kollegengruppe `OKR` und Ziele angesprochen.

(pause: 1)

Gemeinsam haben Sie einen Workshop vereinbart, in der Ella als Lernbegleiterin den Kreis in der Priorisierung und Diskussion über die neuen Ziele unterstützt.

---
![contain](slides/chapter03/Folie12.png)

Heben wir die gesamte Geschichte zurück auf das zu beginn gezeigte Bild des Kreises.

(pause: 1)

Tom hat seinen Kreis erfolgreich in eine Kollegiale kreisinterne Führung umorganisiert. Die Kolleginnen und Kollegen übernehmen nun Teile der Verantwortung und am Ende schafft es Tom sogar wieder, Vorsprung beim Kunden zu erzeugen.

(pause: 1)

Organisationsrollen sind also Klammern um Themengebiete, Aufgaben und Funktionen, die für sich geschlossen betrachtet gemacht werden können.

(pause: 1)

Übrigens Unterstützt Maria aus dem Dienstleistungskreis Sales den Kreis bei der Erstellung von neuen Angeboten und auch bei der Nachverfolgung von laufenden Aufträgen. Frankgalf und Tom haben nun auch wieder etwas mehr Zeit, um über das wesentliche und wichtige zu sprechen.

---
![contain](slides/chapter03/Folie13.png)

Wir haben also schon jetzt die wichtigsten organisatorischen Rollen gezeigt.

(pause: 1)

Der Repräsentant trägt die Verantwortung für die visionäre und gesamtheitliche Entwicklung des Kreises. Er spricht für und vertritt den Kreis gegenüber anderen Koordinationskreisen.

(pause: 1)

Die Ökonomin verantwortet die ökonomischen, also betriebswirtschaftliche Planung und Leistungen des Kreises und dessen Ziele. Er berichtet nach innen wie nach aussen über die wichtigsten Kennzahlen, prüft Kontingente und Laufzeiten und bleibt im stetigen Austausch mit dem Dienstleistungskreis Sales.

(pause: 1)

Der Gastgeber verantwortet die angemessene Arbeitsfähigkeit und die Rahmenbedingungen des Kreises an sich, sowie die Einhaltung der Konstitutionen und vereinbarten Abläufe und Arbeitstreffen. Die Person mit dieser Rolle, ist auch für das Wohlbefinden der Kolleginnen und Kollegen verantwortlich.

(pause: 1)

Die Dokumentarin verantwortet die Informationen, Dokumentationen und das Logbuch über die Arbeit und Entscheidungen des Kreises. Neben der dokumentierten Konstitution selbst, gibt es Aufgaben, wie beispielsweise die regelmäßige Aktualisierung der Projektprofile oder dem Stairway to Change. 

(pause: 1)

Der Kollegenfinder verantwortet die Kreis-Personalplanung, unterstützt die Personalbeschaffung und stellt Qualität sowie Aktualität der Stellenanzeigen sicher. Dieser ist in einem regelmäßigen Austausch mit Human Relations.

(pause: 1)

Der Marktstratege hat die aktuellen und potentiellen Kunden des Kreises immer im Blick und erkennt den aktuellen und auch zukünftigen Bedarf. Daher ist er nicht nur fachlich sondern auch technisch immer auf der Höhe um wichtige strategische Entscheidungen für den Kreis zu benennen.

(pause: 1)

Ein Lernbegleiter sorgt für Möglichkeiten zur Reflexion und Weiterentwicklung des Kreises, sammelt Herausforderungen und klärt Fragen und kann extern von einem anderen spezialisierten Kreis beansprucht werden. Hier in unserem Beispiel Ella, als Lernbegleiterin für die Ziele des Kreises.

---
![contain](slides/chapter03/Folie14.png)

Diese Rollen sind global verfügbare Organisationsrollen und werden in allen Kreisen soweit nötig in der selben Form verwendet. Das bringt vor allem den Vorteil, das ein oder eine Ökonomin die Aufgabe in einem weiteren Kreis, falls gewünscht, ebenfalls übernehmen kann. 

(pause: 1)

Die Inhaber der Rollen treffen sich in regelmäßigen Abständen auch in den Praktikergruppen, um Ihre Erfahrung mit den Aufgaben und Verantwortungen der jeweiligen Rolle austauschen zu können.

---
![contain](slides/chapter03/Folie15.png)

Die gezeigte Rolle der Adobe Expertin ist wie angesprochen eine spezifische Rolle die vom Kreis definiert werden muss. Daher auch eine lokale Rolle. Die Rollenbeschreibung muss jemand externen recht schnell ein Verständnis geben, welche Funktion und Aufgabe mit der Rolle verbunden ist. 

---
![contain](slides/chapter03/Folie16.png)

Kim hatte in unserem fiktiven Beispiel diese Rolle der Adobe Expertin vom Team verliehen bekommen. 

---
![contain](slides/chapter03/Folie17.png)

Um etwas mehr Klarheit über Organisationsrollen zu schaffen, möchten wir diese auch von Fachrollen abgrenzen.

(pause: 1)

Der Geschäftskreis hat sich entschieden, nach aussen und im Sinne des Leistungsportfolios drei Fachrollen zu beschreiben.

(pause: 1)

Der oder die Screen Interaction Designerin, einem Data Flow Engineer und die des Frontend Developers. 

---
![contain](slides/chapter03/Folie18.png)

Kim hat mit Tom beschlossen, das Sie in Ihrer primären Fachrolle als Screen Interaction Designerin arbeitet und vor allem arbeiten möchte. Hierzu bringt Sie alle Fähigkeiten und vor allem am meisten Motivation mit. 

(pause: 1)

Durch Ihre Fähigkeiten kann Sie aber auch als Frontend Entwicklerin eingesetzt werden. Selbst ist Ihr bewusst, das Sie zwar gut ist aber es auch andere gibt, die das wesentlich besser machen können. Tom und Kim einigen sich darauf, das dies Ihre sekundäre Fachrolle ist.

(pause: 1)

Kim sieht sich selbst nicht als Data Flow Engineer, da Ihr dazu einige Fähigkeiten fehlen.

---
![contain](slides/chapter03/Folie19.png)

Für die primäre Rolle hat Kim - hier im fiktiven und natürlich trivialisierten Beispiel - alle notwendigen Fähigkeiten.

---
![contain](slides/chapter03/Folie20.png)

Sie hat natürlich über die Jahre auch andere Dinge erlernt und praktizieren können. Daher kann Sie auch guten gewissens auch bei der Entwicklung von Anwendungen im Frontend unterstützen.

(pause: 1)

Sales und Operations verwendet diese Informationen übrigens vor allem, um bei der Allocation-Phase, also dem Zeitpunkt, an dem in einem konkreten Angebot Kolleginnen und Kollegen zugeordnet werden, die besten und auch verfügbaren Personen zuordnen kann. 

(pause: 1)

Weitere Fachrollen könnten beispielsweise Product Owner, Solution Architekt, Enterprise Architekt oder Messaging Expert sein. Dies muss aber im Unternehmen klar abgestimmt sein und es gilt auch für die Fachrollen, das diese nicht nur dem Kreis sondern auch externen Kolleginnen und Kollegen, beispielsweise in Sales, klar sein muss.

---
![contain](slides/chapter03/Folie21.png)

Lasst uns aber noch ein anderes Thema etwas abgrenzen. Die Bezeichnung der Entwicklungsstufe.

(pause: 1)

Weder die Organisationsrolle, noch die Fachrolle und die Fähigkeiten sind allein ausschlaggebend für die Entwicklungsstufe von Kim. Natürlich gibt es eine engere Bindung der Themen, aber entscheidend sind hier noch einige andere Dimensionen, die uns hier bei esentri sehr wichtig sind. Dazu gibt es aber einen separaten Kurs.

---
![contain](slides/chapter03/Folie22.png)

Kommen wir zum letzten Thema der kreisinternen Struktur. Der Konstitution selbst.

(pause: 1)

Als Konstitution bezeichnen wir den Vorgang und auch die Dokumentation eines Kreises. Die Konstitutionsdokumentation sollte auch kontinuierlich aktualisiert werden.

(pause: 1)

Ziel und Zweck der Dokumentation unserer Organisation ist es, das externe Kolleginnen und Kollegen einen Einblick erhalten können. Dies ist vor allem sehr wichtig, da der Überblick und das Verständnis über die flexible und vielschichtige Organisationsarchitektur sonst recht schnell verloren geht. Neue Kolleginnen und Kollegen können darüber auch recht schnell die wichtigsten Einheiten und Teams kennenlernen.

(pause: 1)

Als erstes ist natürlich der Name und ein Kürzel des Kreises wichtig.

---
![contain](slides/chapter03/Folie23.png)

Das Team sollte sich dann auch mit der Frage auseinander setzen, welche Vision der Kreis verfolgt. Welchen Mehrwert und welchen Vorsprung möchte man mit der neuen Struktur herstellen.

---
![contain](slides/chapter03/Folie24.png)

Wichtig ist es auch, das externe Kolleginnen und Kollegen erkennen, wo ein Beitritt möglich ist. Ist das Team vollständig und wird überhaupt aktuell nach neuen Kolleginnen und Kollegen gesucht? Praktikergruppen haben per se einen sehr offenen und inklusiven Character. Die primären Geschäfts- und auch Dienstleistungskreise sind eher exklusiv.

---
![contain](slides/chapter03/Folie25.png)

Neben den Mitgliedern ist es eben auch wichtig, dem Plenum die aktuelle Rollenvergabe aufzuzeigen. Somit ist für aussenstehende recht schnell klar, wer der primäre Ansprechpartner oder Partnerin für ein spezielles Thema im Kreis ist.

---
![contain](slides/chapter03/Folie26.png)

Das bereits angesprochene Leistungsportfolio soll für die Innen- sowie für die Aussenperspektive gut funktionieren. Was ist es, was unser Kreis für den Markt oder für die wertschöpfenden Kreise leisten kann? Welche Themen sollen im Koordinationskreis geklärt werden?

---
![contain](slides/chapter03/Folie27.png)

Ebenso ist die Dokumentation der ausgehandelten Arbeitstreffen und Werkzeuge oder vereinbarten Prozesse wichtig. Bei offenen Kreisen, wie beispielsweise den Praktikergruppen, hilft diese Information, den nächstmöglichen Teilnahmetermin selbstständig zu finden.

(pause: 1)

Bei koordinativen oder wertschöpfenden Kreisen sprechen wir auch von einem vereinbarten Vertrag. Wie oft und wann finden die Arbeitstreffen statt? Wo werden Ziele nachgehalten und dokumentiert? Wer ist für welchen Arbeitsschritt oder Prozess verantwortlich.

---
![contain](slides/chapter03/Folie28.png)

Kommen wir zum letzten sehr wichtigen Thema der Konstitution. 

In unserer internen Dokumentation gibt es einige weitere sinnvolle Artefakte, die in der Kreiskonstitution aufgenommen werden können.

(pause: 1)

Bei den Kooperationen geht es wie auch schon im Kapitel 2 um die Abbildung des Netzwerkes. Mit welchen Kreisen findet eine engere Zusammenarbeit statt oder welche Personen unterstützen unseren Kreis. Wer ist Sponsor oder Vorgesetzter des Kreises und in welchen Koordinationskreisen sind wir aktiv.

(pause: 1)

Werden diese regelmäßig dokumentiert, werden Zusammenhänge im Unternehmen und den einzelnen Kreisen für alle transparent. Dies erzeugt nicht nur Klarheit sondern fördert auch die Möglichkeit, das sich jeder mit seinen Fähigkeiten und Interessen flexibel und kreativ einbringen kann.

---
![contain](slides/chapter03/Folie29.png)

Wir möchten das Kapitel kurz zusammenfassen.

(pause: 1)

Wir haben über die Verteilung von Verantwortung und die damit verbundenen und verfügbaren Organisationsrollen gezeigt.

(pause: 1)

Die einzelnen Organisationsrollen und deren Funktionen sowie damit verbundene Aufgaben wurden vorgestellt.

(pause: 1)

Wir haben die Organisatorischen Rollen auch von Fachrollen und Fähigkeiten abgegrenzt.

(pause: 1)

In einem kleineren Beispiel haben wir auch die Entwicklungsstufe von den Rollen und Fähigkeiten abgegrenzt.

(pause: 1)

Zum Ende haben wir die wichtigsten Artefakte erklärt, die in einer Konstitution, also der Gründung eines neuen Kreises entscheidend sind.















