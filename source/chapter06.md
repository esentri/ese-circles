---
size: 1080p
canvas: white
transition: crossfade 0.2
background: music/background.mp3 0.05 fade-out fade-in
theme: light
subtitles: embed
voice: Ilse
---
![contain](slides/chapter06/Folie1.png)

(pause: 3)

Wir möchten die besprochenen Themen aus den Kapiteln nochmal kurz zusammenfassen.

Erstens: Wir haben im Kurs klargestellt, warum die Verteilung der Aufträge für die Organisation wichtig ist.

Zweitens: Wir haben Aufträge für Expertendiensleistungen genauer erklärt, und Begriffe in diesem Zusammenhang erläutert und die Schritte auf hoher Ebene betrachtet.

Drittens: Wir haben jeden Prozessschritt im Detail angeschaut und die Aufgaben auch in einer zeitlichen Abfolge betrachtet.

Viertens: Wir haben nochmals zusammengefasst, welcher Kreis in dem Prozess welche Verantwortung trägt.

Fünftens: Wir haben im letzten Kapitel kurz die Unterlagen und Werkzeuge erklärt, die für eine erfolgreiche Durchführung der Expertendiensleistungen notwendig sind.

---
![contain](slides/chapter06/Folie2.png)


Herzlichen Glückwunsch, Ihr habt den Kurs hiermit abgeschlossen. 

Bitte vergesst nicht, Feedback zum Kurs zu geben, damit wir diesen verbessern können.

(pause: 3)

---
